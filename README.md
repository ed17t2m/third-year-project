# Third Year Project

## Running notebooks in Google Colab
Once imported to google colab, you need to enable GPU usage to run the models provided.
This can be done by: 
* In Colab, navigating to the edit menu, and selecting Notebook Settings
* Under Hardware Accelerator, select GPU
This should enable colab to use a GPU for any code being executed.
