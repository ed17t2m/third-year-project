# -*- coding: utf-8 -*-
"""
@author: Thomas Marshall
"""

import sys
import numpy as np
import pandas as pd
from nltk.corpus import stopwords

input_file = open(sys.argv[1], "r", encoding="latin1")
output_name = "stopwords-"+sys.argv[1]

stopwords_list = stopwords.words('english')
dataset = pd.read_csv(sys.argv[1],header=0)

dataset['tweet'] = dataset['tweet'].apply(lambda x: ' '.join([word for word in x.split() if word not in stopwords_list]))
print(dataset)
dataset.to_csv(output_name)
