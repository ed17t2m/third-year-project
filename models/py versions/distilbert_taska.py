# -*- coding: utf-8 -*-
"""DistilBERT-taskA.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1BV44DMl-8H2F7x3KuwgNA_Yhgzt5Lij9
"""

!pip install transformers
!pip install datasets
!pip install pandas
!pip install tensorflow-gpu
!pip install sklearn

from transformers import AutoTokenizer, TFDistilBertForSequenceClassification, TFTrainer, TFTrainingArguments
from datasets import Dataset
import numpy as np
import pandas as pd
import requests
import tensorflow as tf
import io
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score

print("import completed")
print(tf.__version__)

# test to ensure that a GPU is available for use
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
  raise SystemError('GPU device not found')
print('Found GPU at: {}'.format(device_name))

def convertLabelsToIds(X):
  label_x = []
  for x in X:
    if x == "OFF":
      label_x.append(1)
    else:
      label_x.append(0)
  return(label_x)

"""SELECTION OF TEST SET TO BE USED


*   Set test set to 0 for test set with stopwords removed
*   Set test set to 1 for original test set




"""

test_set = 0

#import data for training set and task 1 test set from git repo
training_url = 'https://gitlab.com/ed17t2m/third-year-project/-/raw/master/data%20sets/stopword%20test%20sets/stopwords-olid-training-set.csv'

if test_set == 0:
  test_url = 'https://gitlab.com/ed17t2m/third-year-project/-/raw/master/data%20sets/stopword%20test%20sets/stopwords-fulltestset-a.csv'
elif test_set == 1:
  test_url = 'https://gitlab.com/ed17t2m/third-year-project/-/raw/master/data%20sets/original%20test%20sets/fulltestset-a.csv'
else:
  raise SystemError('Error: Test set not specified')

training_req = requests.get(training_url).content
test_req = requests.get(test_url).content

training_df = pd.read_csv(io.StringIO(training_req.decode('utf-8')))
test_df = pd.read_csv(io.StringIO(test_req.decode('utf-8')))

# initialise the tokenizer
tokenizer = AutoTokenizer.from_pretrained('distilbert-base-uncased', do_lower_case=True)

#convert the pandas dataframes in Transformer Datasets
training_dataset = Dataset.from_pandas(training_df)
test_dataset = Dataset.from_pandas(test_df)

test_labels = test_dataset['label']

# create a training and validation set from the training set
train_tweets, validation_tweets, train_labels, val_labels = train_test_split(
    training_dataset['tweet'],training_dataset['subtask_a'], test_size=.2
    )

train_labels = convertLabelsToIds(train_labels)
val_labels = convertLabelsToIds(val_labels)
test_labels = convertLabelsToIds(test_labels)

train_encodings = tokenizer(train_tweets, padding=True, truncation=True, return_tensors="tf")
val_encodings = tokenizer(validation_tweets, padding=True, truncation=True, return_tensors='tf')
test_encodings = tokenizer(test_dataset['tweet'], padding=True, truncation=True, return_tensors="tf")

train_dataset = tf.data.Dataset.from_tensor_slices((
    dict(train_encodings),
    train_labels
))

validation_dataset = tf.data.Dataset.from_tensor_slices((
    dict(val_encodings),
    val_labels
))

test_dataset = tf.data.Dataset.from_tensor_slices((
    dict(test_encodings),
    test_labels
))

def compute_metrics(p):
  pred, labels = p
  pred = np.argmax(pred, axis=1)

  accuracy = accuracy_score(y_true=labels, y_pred=pred)
  recall = recall_score(y_true=labels, y_pred=pred)
  precision = precision_score(y_true=labels, y_pred=pred)
  f1 = f1_score(y_true=labels, y_pred=pred)

  return {"accuracy":accuracy, "precision":precision, "recall": recall, "f1":f1}

training_args = TFTrainingArguments(
    output_dir='./results',
    num_train_epochs=3,
    per_device_train_batch_size=16,
    per_device_eval_batch_size=24,
    warmup_steps=500,
    weight_decay=0.01,
    learning_rate=0.00002,
    logging_dir='./logs',
    logging_steps=10,
)

with training_args.strategy.scope():
  model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased')

trainer = TFTrainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=validation_dataset,
    compute_metrics = compute_metrics,
)

trainer.train()

def ConfusionMatrix(pred, actual):
  eval = np.subtract(actual, pred)
  
  false_pos = 0
  false_neg = 0
  true_pos = 0
  true_neg = 0

  for i in range(eval.size):
    if eval[i] == -1:
      false_pos += 1
    elif eval[i] == 1:
      false_neg += 1
    else:
      if actual[i] == 1:
        true_pos += 1
      else:
        true_neg += 1
  
  return {"true_pos": true_pos, "true_neg": true_neg, "false_pos": false_pos, "false_neg": false_neg}

def evalModel(pred, labels):
  accuracy = accuracy_score(y_true=labels, y_pred=pred)
  recall = recall_score(y_true=labels, y_pred=pred)
  precision = precision_score(y_true=labels, y_pred=pred)
  f1 = f1_score(y_true=labels, y_pred=pred)

  return {"accuracy":accuracy, "precision":precision, "recall": recall, "f1":f1}

raw_pred, _, _ = trainer.predict(test_dataset)
y_pred = np.argmax(raw_pred, axis=1)

actual_labels = np.asarray(test_labels)

confusion = ConfusionMatrix(y_pred, actual_labels)

eval = evalModel(y_pred, actual_labels)

print(confusion)
print(eval)